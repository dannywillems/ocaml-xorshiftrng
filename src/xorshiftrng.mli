module State : sig
  type t

  val copy : t -> t

  val make : int array -> t

  val int32 : t -> Int32.t

  val int : t -> int

  val bool : t -> bool
end

val int : int -> int
