module State = struct
  type t =
    { mutable x : int32;
      mutable y : int32;
      mutable z : int32;
      mutable w : int32
    }

  let copy t = { x = t.x; y = t.y; z = t.z; w = t.w }

  let make (seed : int array) : t =
    assert (Array.length seed = 4) ;
    let x = seed.(0) in
    let y = seed.(1) in
    let z = seed.(2) in
    let w = seed.(3) in
    let x_u32 = Int32.of_int x in
    let y_u32 = Int32.of_int y in
    let z_u32 = Int32.of_int z in
    let w_u32 = Int32.of_int w in
    { x = x_u32; y = y_u32; z = z_u32; w = w_u32 }

  let int32 random =
    let x = random.x in
    let t = Int32.to_int x lxor (Int32.to_int x lsr 11) in
    random.x <- random.y ;
    random.y <- random.z ;
    random.z <- random.w ;
    random.w <-
      Int32.of_int
        ( Int32.to_int random.z
        lxor (Int32.to_int random.z lsl 19)
        lxor (t lxor (t lsl 8)) ) ;
    random.w

  let rec int random =
    let y = int32 random in
    let x = Int32.to_int y lsr 1 in
    if x < 0 then int random else x

  let bool random = int random mod 2 == 0
end

external random_seed : unit -> int array = "caml_sys_random_seed"

let state : State.t = State.make (Array.sub (random_seed ()) 0 4)

let int bound =
  if bound > 0x3FFFFFFF || bound <= 0 then invalid_arg "Xorshiftrng.int"
  else
    let v = State.int state in
    v mod bound
