Xorshift RNG in OCaml
=============================

**THIS IS A WIP - Do not use**

An Xorshift random number generator.

The Xorshift algorithm is not suitable for cryptographic purposes
but is very fast. If you do not know for sure that it fits your
requirements, use a more secure one such as the standard module `Random`.

See Marsaglia, George (July 2003). ["Xorshift
RNGs"](https://www.jstatsoft.org/v08/i14/paper). *Journal of Statistical
Software*. Vol. 8 (Issue 14).

Try to map the implementation of xorshift rng in Rust.

Map the Random module interface to be used in tests.
